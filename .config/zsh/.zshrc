# Environment variables.
if [ -f "${HOME}/.profile" ]; then
	. "${HOME}/.profile"
fi

# Lines configured by zsh-newuser-install
HISTFILE="${XDG_STATE_HOME}/zsh/histfile"
HISTSIZE=5000
SAVEHIST=5000
setopt autocd extendedglob nomatch
unsetopt beep
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' menu select=1
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle :compinstall filename "${XDG_CONFIG_HOME}/zsh/.zshrc"

autoload -Uz compinit promptinit vcs_info
compinit -d "${XDG_CACHE_HOME}/zsh/zcompdump-${ZSH_VERSION}"
promptinit
# End of lines added by compinstall
zstyle ':completion:*' cache-path "${XDG_CACHE_HOME}/zsh/zcompcache"

# set PATH so it includes user's private bin if it exists
if [ -d "${HOME}/bin" ]; then
	PATH="${HOME}/bin:${PATH}"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "${HOME}/.local/bin" ]; then
	PATH="${HOME}/.local/bin:${PATH}"
fi

# Alias definitions.
if [ -f "${XDG_CONFIG_HOME}/zsh/aliases" ]; then
	. "${XDG_CONFIG_HOME}/zsh/aliases"
fi

# Docker setup definitions.
if [ -f "${XDG_CONFIG_HOME}/zsh/docker" ]; then
	. "${XDG_CONFIG_HOME}/zsh/docker"
fi

# Keybindings.
if [ -f "${XDG_CONFIG_HOME}/zsh/keybindings" ]; then
	. "${XDG_CONFIG_HOME}/zsh/keybindings"
fi

# Exports.
if [ -f "${XDG_CONFIG_HOME}/zsh/exports" ]; then
	. "${XDG_CONFIG_HOME}/zsh/exports"
fi

# Prompt definitions.
if [ -f "${XDG_CONFIG_HOME}/zsh/prompt" ]; then
	. "${XDG_CONFIG_HOME}/zsh/prompt"
fi
