# Dotfiles
This repository contains my personal dot files managed via git.

Inspired by [hackernews][hackernews]

To clone the repository
```sh
git clone --separate-git-dir="${HOME}/.local/share/dotfiles.git" git@gitlab.com:olliver/dotfiles.git "${HOME}"
```

If this fails due to 'existing files' in the home directory, instead clone
into a temporary directory first, copy submodules, update submodules, checkout
the new files and remove the temporary location.
```sh
git clone --separate-git-dir="${HOME}/.local/share/dotfiles.git" git@gitlab.com:olliver/dotfiles.git "${HOME}/tempdots"
mv "${HOME}/tempdots/.gitmodules" "${HOME}/"
git --git-dir="${HOME}/.local/share/dotfiles.git" submodule update --init [--recursive]
git --git-dir="${HOME}/.local/share/dotfiles.git" checkout "${HOME}"
rm -f -r "${HOME}/tempdots"
```

To best manage this repository an alias to git is needed to help git use
the separated git dir. This is part of [.zsh/aliases](.zsh/aliases), and
should be available on launching a new shell.
```sh
alias dotfiles="$(command -v git) --git-dir=\"${HOME}/.local/share/dotfiles.git/\" --work-tree=\"${HOME}\""
```

Finally to not show and track untracked files, ignore those.
```sh
dotfiles config status.showUntrackedFiles no
```

[hackernews]: https://news.ycombinator.com/item?id=11071754
